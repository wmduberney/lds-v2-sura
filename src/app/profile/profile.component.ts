import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.sass'],
  providers:[UserService]
})
export class ProfileComponent implements OnInit {

    baseURL = this.userService.apiData.STR_SERVER_URL+'WebAPI/';
    defaultPicture = 'assets/img/profile.png';
    user = {
      "Name":'',
      "Email":'',
      "Follows":'',
      "Followers":'',
      "Votes":'',
      "Summary":'',
      "Picture":''
    };
    followersInfo = {
      "Follows":'',
      "Followers":'',
      "Votes":''
    };
    company = {};
    city = {};
    posts = [];
    gettingMorePosts = false;
    currentPage = 0;
    constructor(private router: Router,private userService:UserService ) {
      this.userService.getFollowersInfo().then(followers=>{
        this.followersInfo.Follows = followers['Follows'];
        this.followersInfo.Followers = followers['Followers'];
        this.followersInfo.Votes = followers['Votes'];
      });
      this.user = this.userService.userData;
      this.city = this.user['City'];
      this.company = this.user['Company'];
      // this.userService.getCompany().then(company=>this.company = company);
      // this.userService.getCity().then(city=>this.city = city);
      this.userService.getUserPosts(this.currentPage).then(posts=>{this.posts.push.apply(this.posts,posts)});
    }
    ngOnInit() {
      window.addEventListener('scroll', this.scroll, true);
    }

    ngOnDestroy() {
      window.removeEventListener('scroll', this.scroll, true);
    }

    scroll = (e): void => {
      if ($(window).scrollTop() + $(window).height() > $(document).height() - 250) {
        if (!this.gettingMorePosts) {
          this.gettingMorePosts = true;
          this.currentPage += 1;
          this.userService.getUserPosts(this.currentPage).then(posts => {
            this.posts.push.apply(this.posts, posts);
            this.gettingMorePosts = false;
          });
        }
      }
    };
    showCommentsForPost(postId){
      $('#publication_'+postId+' .commentsHolder').toggleClass('hidden');
    }
    showMoreCommentsForPost(postId){
      this.userService.getCommentsForPost(postId).then(comments=>{
        let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
        this.posts[objIndex].ListComment = comments;
      });
      $('#publication_'+postId+' .showMoreText').addClass('hidden');
    }
    likePost( postId) {
      this.userService.likePost(postId).then(count => {
        let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
        this.posts[objIndex].CountLike = count;
      });
    }
    savePost(postId){
      this.userService.savePost(postId).then(post => {
        let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
        this.posts[objIndex] = post;
      });
    }
    commentPosts(postId){
      var comment = $('#publication_'+postId+' .comment').val();
      if(!comment){
        return;
      }
      this.userService.commentPosts(postId,comment).then(post => {
        let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
        this.posts[objIndex] = post;
      });
    }

    reportContent(postId){
      this.router.navigate(['/report',postId]);
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealTimeLegendsComponent } from './real-time-legends.component';

describe('RealTimeLegendsComponent', () => {
  let component: RealTimeLegendsComponent;
  let fixture: ComponentFixture<RealTimeLegendsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealTimeLegendsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealTimeLegendsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

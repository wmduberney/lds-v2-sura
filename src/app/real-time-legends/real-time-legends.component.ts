import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-real-time-legends',
  templateUrl: './real-time-legends.component.html',
  styleUrls: ['./real-time-legends.component.sass']
})
export class RealTimeLegendsComponent implements OnInit {

	publicationItem = [
    {
      image: 'assets/img/img-profile.jpg',
      name: 'Jhojan Guerrero',
      publicationImage: 'assets/img/bg-publications.jpg',
      comment: 'Esto es una prueba',
      likes: '76',
      comments: '18'
    },
    {
      image: 'assets/img/img-profile.jpg',
      name: 'Sebastian Vasquez',
      publicationImage: 'assets/img/bg-publications.jpg',
      comment: 'Probando Angular 4',
      likes: '20',
      comments: '3'
    },
    {
      image: 'assets/img/img-profile.jpg',
      name: 'Paola Medina',
      publicationImage: 'assets/img/bg-publications.jpg',
      comment: 'En el trabajo',
      likes: '7',
      comments: '14'
    },
    {
      image: 'assets/img/img-profile.jpg',
      name: 'Jhojan Guerrero',
      publicationImage: 'assets/img/bg-publications.jpg',
      comment: 'Esto es una prueba',
      likes: '76',
      comments: '18'
    },
    {
      image: 'assets/img/img-profile.jpg',
      name: 'Sebastian Vasquez',
      publicationImage: 'assets/img/bg-publications.jpg',
      comment: 'Probando Angular 4',
      likes: '20',
      comments: '3'
    },
    {
      image: 'assets/img/img-profile.jpg',
      name: 'Paola Medina',
      publicationImage: 'assets/img/bg-publications.jpg',
      comment: 'En el trabajo',
      likes: '7',
      comments: '14'
    }
  ]

  constructor() { }

  ngOnInit() {
  }

}

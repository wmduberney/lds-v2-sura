import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SavedContentsComponent } from './saved-contents.component';

describe('SavedContentsComponent', () => {
  let component: SavedContentsComponent;
  let fixture: ComponentFixture<SavedContentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SavedContentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SavedContentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

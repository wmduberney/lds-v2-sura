import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user/user.service';
import { Router } from '@angular/router';
import { UserDataComponent } from '../user-data/user-data.component';
declare var $:any;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.sass'],
  providers:[UserService,UserDataComponent]
})
export class UserProfileComponent implements OnInit {

  id: number;
  private sub: any;
  // user = {
  //   "Name":'',
  //   "Email":'',
  //   "Follows":'',
  //   "Followers":'',
  //   "Votes":'',
  //   "Summary":'',
  //   "Picture":'',
  //   "Company":'',
  //   "City":''
  // };
  user;
  followers;
  posts = [];
  isVoted = {};
  gettingMorePosts = false;
  currentPage = 0;
  isFollowed = false;
  message='';
  constructor(private router: Router,private route: ActivatedRoute,private userService:UserService,private userData:UserDataComponent) {

  }

  ngOnInit() {

    this.sub = this.route.params.subscribe(params => {
       this.id = +params['id'];
       this.userService.getOtherUserInfo(this.id).then(user=>{
         this.user = user;
         this.userService.getOtherUserFollowrs(this.id).then(followers=>{
           this.followers = followers;
         });
         this.userService.isVoted(this.id).then(isVoted=>{
           this.isVoted = isVoted;
         })
         this.userService.getFollowers().then(followers=>{
           for(var i in followers){
             if(followers[i]['UserId']==this.id){
               this.isFollowed = true;
                 $('[data-toggle="tooltip"]').tooltip();

               break;
             }
           }
         })
         this.userService.getOtherUserPosts(this.id,this.currentPage).then(posts=>{this.posts.push.apply(this.posts,posts);});
       });
    });
    window.addEventListener('scroll', this.scroll, true);
    $('[data-toggle="tooltip"]').tooltip();

  }
  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (e): void => {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 250) {
      if (!this.gettingMorePosts) {
        this.gettingMorePosts = true;
        this.currentPage += 1;
        this.userService.getOtherUserPosts(this.id,this.currentPage).then(posts => {;
          this.posts.push.apply(this.posts, posts);
          this.gettingMorePosts = false
        });
      }
    }
  };
  showCommentsForPost(postId){
    $('#publication_'+postId+' .commentsHolder').toggleClass('hidden');
  }
  showMoreCommentsForPost(postId){
    this.userService.getCommentsForPost(postId).then(comments=>{
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex].ListComment = comments;
    });
    $('#publication_'+postId+' .showMoreText').addClass('hidden');
  }
  likePost( postId) {
    this.userService.likePost(postId).then(count => {
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex].CountLike = count;
    });
  }
  savePost(postId){
    this.userService.savePost(postId).then(post => {
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex] = post;
    });
  }
  commentPosts(postId){
    var comment = $('#publication_'+postId+' .comment').val();
    if(!comment){
      return;
    }
    this.userService.commentPosts(postId,comment).then(post => {
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex] = post;
    });
  }
  goBack(){
    window.history.back()
  }
  reportContent(postId){
    this.router.navigate(['/report',postId]);
  }
  followUser(userId){
    this.isFollowed = !this.isFollowed;

    this.userService.followUser(userId,this.isFollowed).then(res=>{
      this.userService.getFollowers().then(followers=>{
        for(var i in followers){
          if(followers[i]['UserId']==this.id){
            this.isFollowed = true;
            break;
          }
        }
      })
      this.userService.getOtherUserFollowrs(this.id).then(followers=>{
        this.followers = followers;
      });

    })
  }
  vote(userId){
    var voteMessage = $('.voteMessage').val();
    if(!voteMessage){
      this.message = 'Por favor escribe las razones de tu voto.';
      return;
    }
    this.userService.voteForUser(userId,voteMessage);
  }
}

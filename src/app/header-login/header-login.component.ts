import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-login',
  templateUrl: './header-login.component.html',
  styleUrls: ['./header-login.component.sass'],
  providers:[UserService]
})
export class HeaderLoginComponent  {
  respNotifications;
  notifications;

  constructor(private userService: UserService, private router:Router) {
    this.userService.getNotificatios().then(data=>{
      this.respNotifications = data;
      if (this.respNotifications.Result){
        this.notifications = this.respNotifications.ArrayNotificationByuser;
      }
    }, err=>{
      console.log("Err");
      console.log(err);
    })
  }
  menu =  [
    {
      active: false,
      title: 'Opciones',
      image: 'assets/img/icons/nav-1.png',
      imageActive: 'assets/img/icons/nav-2.png',
      url: '',
      submenu: [
        {
          urlSubmenu: '/#/challenges',
          title: 'Retos'
        },
        {
          urlSubmenu: '/#/awards',
          title: 'Premios'
        },
        {
          urlSubmenu: '/#/challenges',
          title: 'Mensajes'
        },
        {
          class: 'line-separator'
        },
        // {
        //   title: 'Contenidos que me gustan'
        // },
        {
          title: 'Editar perfil',
          urlSubmenu: '/#/update-data',
        },
        {
          urlSubmenu: '/#/saved-contents',
          title: 'Contenidos guardados'
        },
        {
          urlSubmenu: '/#/saved-contents',
          title: 'Contenidos que me gustan'
        },
        {
          urlSubmenu: '/#/saved-contents',
          title: 'Historial de puntos ganados'
        },
        {
          urlSubmenu: '/#/saved-contents',
          title: 'Historial de redenciones'
        },
        {
          urlSubmenu: '/#/saved-contents',
          title: 'Términos y condiciones'
        },
        {
          urlSubmenu: '/#/saved-contents',
          title: 'Preguntas frecuentes'
        },
        {
          urlSubmenu: '/#/saved-contents',
          title: 'Acerca de leyendas del servicio'
        },
        {
          title: 'Cerrar sesión',
          click: 'logout',
        }





        // {
        //   title: 'Acerca de leyenda del servicio'
        // },
        // {
        //   urlSubmenu: '/#/terminosycondiciones',
        //   title: 'Términos y condiciones'
        // },
        // {
        //   title: 'Preguntas frecuentes',
        //   urlSubmenu: '/#/faq',
        // },
        // {
        //   title: 'Cerrar sesión',
        //   click: 'logout',
        // }
      ]
    },
    {
      active: this.router.url == '/legend' ? true:false,
      title: 'Leyenda ejemplar',
      image: 'assets/img/icons/example-1.png',
      imageActive: 'assets/img/icons/example-2.png',
      url: '/#/legend',
      submenu: []
    },
    {
      active:  this.router.url == '/profile' ? true:false,
      title: 'Mi perfil',
      image: 'assets/img/icons/profile-1.png',
      imageActive: 'assets/img/icons/profile-2.png',
      url: '/#/profile',
      submenu: []
    },
    {
      active:  this.router.url == '/real-time-legends' ? true:false,
      title: 'Leyendas del servicio',
      image: 'assets/img/icons/ico-legends-1.png',
      imageActive: 'assets/img/icons/ico-legends-2.png',
      url: '/#/real-time-legends',
      submenu: []
    },
    {
      active: this.router.url == '/dashboard' ? true:false,
      title: 'Inicio',
      image: 'assets/img/icons/home-1.png',
      imageActive: 'assets/img/icons/home-2.png',
      url: '/#/dashboard',
      submenu: []
    }    
  ];

  // active(i){
  //   this.menu.map( x => x.active = false);
  //   this.menu[i].active = true;
  // }
  //
  // overactive(i){
  //   this.menu[i].active = true;
  // }




  submenuclick(e,sub){
    if(sub) e.preventDefault();
    if(sub=='logout'){
      this.userService.logout();
      this.router.navigate(['login']);
    }
  }


}

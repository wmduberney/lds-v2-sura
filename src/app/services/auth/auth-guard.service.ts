import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from '../../services/user/user.service';

@Injectable()
export class AuthGuardService implements CanActivate{

  constructor(public router: Router, private userService:UserService ) {
  }
  canActivate( route : ActivatedRouteSnapshot ): boolean {
    let path = route.url[0].path;
    if(path == 'login'){
      if (this.isAuthenticated()) {
        this.router.navigate(['home']);
        return false;
      }
      return true;
    }

    if (!this.isAuthenticated()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }

  private isAuthenticated(): any {
      var tokenInfo = localStorage.getItem('tokenInfo');
      if(tokenInfo){
        tokenInfo = JSON.parse(tokenInfo);
        var expiresIn = tokenInfo['.expires'];
        var date = +new Date(expiresIn);
        var now = +new Date();
        return date - now >0? true: false;
      }
      return false;
  }
}

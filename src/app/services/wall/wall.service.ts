import { Injectable } from '@angular/core';
import { Http, URLSearchParams, QueryEncoder } from '@angular/http';
import 'rxjs/add/operator/map';

class GhQueryEncoder extends QueryEncoder {
    encodeKey(k: string): string {
        k = super.encodeKey(k);
        return k.replace(/\+/gi, '%2B');
    }
    encodeValue(v: string): string {
        v = super.encodeKey(v);
        return v.replace(/\+/gi, '%2B');
    }
}


@Injectable()
export class WallService {

  constructor(public http: Http) { }


  getPost(){
    let data = new URLSearchParams('', new GhQueryEncoder());

    data.append("Token", "demo");
    let url = "http://makedigital.com.co/clientes/demo.php";

    let promesa = new Promise ( (resolve, reject)=> {
      this.http.post(url, data)
      .map( resp => resp.json() )
      .subscribe( (data)=>{
        resolve(data);
      })
    });
    return promesa;

  }


  likePost(){
    let data = new URLSearchParams('', new GhQueryEncoder());

    data.append("Token", "demo");
    let url = "http://makedigital.com.co/clientes/demo.php";

    let promesa = new Promise ( (resolve, reject)=> {
      this.http.post(url, data)
      .map( resp => resp.json() )
      .subscribe( (data)=>{
        resolve(data);
      })
    });
    return promesa;

  }
}

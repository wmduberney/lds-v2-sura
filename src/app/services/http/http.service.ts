import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { ApiData } from '../../classes/strings';

import 'rxjs/add/operator/toPromise';





@Injectable()
export class HttpService {

  apiData = new ApiData;

  constructor(private http: Http) { }

  getData(server, url, params) {
    var paramsUrl = this.addParams(params);
    return this.http.get(server + url + "?" + paramsUrl).toPromise()
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }

  putData(server, url, body, head) {
    var headers = new Headers(head);
    var options = new RequestOptions({ headers: headers });
    return this.http.put(server + url, body, options).toPromise()
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }

  postData(server, url, body, head) {
    var headers = new Headers(head);
    var options = new RequestOptions({ headers: headers, method:'POST' });

    return this.http.post(server + url, body, options).toPromise()
      .then(response => {
        return Promise.resolve(response);
      })
      .catch(error => {
        return Promise.reject(error);
      });
  }

  addParams(params) {
    var str = "";
    for (var i = 0; i < params.length; i++) {
      if (i > 0) {
        str += "&";
      }
      str += params[i].data;
      str += "="
      str += params[i].value;
    }
    return str;
  }

}

import { Injectable, OnInit } from '@angular/core';
import { HttpService } from '../http/http.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ApiData } from '../../classes/strings';

declare var $: any;

@Injectable()
export class UserService implements OnInit {

  public apiData = new ApiData;

  private publicToken = localStorage.getItem('publicToken') || ''
  private privateToken = localStorage.getItem('token') || '';
  private tokenInfo = JSON.parse(localStorage.getItem('tokenInfo')) || null;
  private userBasicInfo = JSON.parse(localStorage.getItem('userBasicInfo')) || null;
  public userData = JSON.parse(localStorage.getItem('userBasicInfo')) || null;
  public isLoggedIn = localStorage.getItem('token') ? true : false;
  public followers = [];
  constructor(
    private httpSrvc: HttpService,
    private http: Http
  ) {
    if(!this.isLoggedIn) return;
    this.getUserInfo().then(info => {this.userData = info;});
    this.getPublicToken();
  }
  private getOptions(){
    if(!this.privateToken) return false;
    var header = {
      'Authorization': 'bearer ' + this.privateToken,
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    var headers = new Headers(header);
    return new RequestOptions({ headers: headers });
  }
  ngOnInit() { }
  getPublicToken() {
    var publicdata = [
      {
        data: 'grant_type',
        value: 'password'
      },
      {
        data: 'username',
        value: 'BigSocialNetwork'
      },
      {
        data: 'password',
        value: 'Big2017#'
      }
    ];
    var header = {
      'content-type': 'application/x-www-form-urlencoded'
    }
    var data = this.httpSrvc.addParams(publicdata);

    return this.httpSrvc.postData(this.apiData.STR_SERVER_URL, this.apiData.STR_PUBLIC_TOKEN_URL, data, header)
      .then(res => {
        this.publicToken = res.json().access_token;
        localStorage.setItem('publicToken',this.publicToken);
        return Promise.resolve(res);
      })
      .catch(res => {
        return Promise.reject(res);
      })
  }
  getTermsAndConditions(){
      var header = {
        'Authorization': 'bearer ' + this.publicToken,
        'Content-Type': 'application/x-www-form-urlencoded'
      }
      var headers = new Headers(header);
      var options = new RequestOptions({ headers: headers });
      var data = {
        "ParameterId": 1,
        "ProgramId": 1
      }
      var str = Object.keys(data).map(function(key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
      }).join('&');
      return new Promise((resolve, reject) => {
        this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ParameterAPI/TermsAndConditions", str, options)
          .map(resp => resp.json())
          .subscribe((data) => {
            resolve(data['Value']);
          })
      });
  }
  getFAQs(){
    var header = {
      'Authorization': 'bearer ' + this.publicToken,
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    var headers = new Headers(header);
    var options = new RequestOptions({ headers: headers });
    var id= 1;
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/FaqAPI/GetFAQS/"+id, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['ArrayFAQ']);
        })
    });

  }
  login(data) {
    var header = {
      'Authorization': 'bearer ' + this.publicToken,
      'Content-Type': 'application/x-www-form-urlencoded'
    }
    var headers = new Headers(header);
    var options = new RequestOptions({ headers: headers, method:'POST' });
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + this.apiData.STR_LOGIN_URL, str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          if(data['Result']){
            var tokenData = {
              grant_type:"password",
              username:data['User']['Guid'],
              password:data['User']['Dni']
            }
            this.getPrivateToken(tokenData)
            .then(res =>{
              localStorage.setItem('userBasicInfo', JSON.stringify(data['User']));
              this.userData = data['User'];
              this.getUserInfo().then(info =>{
                this.userData = info
                resolve(data);
              });
            });
          }else{
              resolve(data);
          }
        })
    });

  }
  getUserInfo() {
    var options = this.getOptions();
    var basic = JSON.parse(localStorage.getItem('userBasicInfo'));
    if(!basic){
      return new Promise((resolve,reject)=>{
        return resolve({});
      })
    };
    let data = {
      'userId': basic['UserId'],
      'width': 150
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/GetUser", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          localStorage.setItem('userBasicInfo', JSON.stringify(data));
          this.userData = data
          resolve(data);
        })
    });
  }
  isVoted(userId){
    var options = this.getOptions();

    let data = {
      "UserId": this.userData.UserId,
      "UserVotedId": userId
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/UserIsVoted", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['Result']);
        })
    });
  }
  getFollowers(){
    var options = this.getOptions();

    let data = {
      "UserId": this.userData.UserId,
      "Width": "1",
      "Height": "1",
      "PageIndex": 0,
      "PageSize": 100
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/GetUserFollow", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['UserArray']);
        })
    });

  }
  followUser(userId,isFollow){
    var options = this.getOptions();

    let data = {
      "UserId": this.userData.UserId,
      "UserFollowId": userId,
      "IsFollow":isFollow
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/SaveUserFollow", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        })
    });
  }
  voteForUser(userId,voteMessage){
    var options = this.getOptions();

    let data = {
      "UserId": this.userData.UserId,
      "UserIdVote": userId,
      "Description":voteMessage,
      "IsActive":true
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/SaveUserVote", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        })
    });
  }
  getOtherUserInfo(id){
    var options = this.getOptions();
    let data = {
      'userId': id,
      'width': 150
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/GetUser", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
            resolve(data);
        })
    });
  }
  getOtherUserFollowrs(id){
    var options = this.getOptions();

    return new Promise((resolve, reject) => {
          this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/GetProfileInfoByUserId/" + id, options)
            .map(resp => resp.json())
            .subscribe((data) => {
              resolve(data);
        })
      });
  }
  logout() {
    this.privateToken = '';
    this.isLoggedIn = false;
    this.userData = null;
    this.tokenInfo = null;
    localStorage.removeItem('token');
    localStorage.removeItem('tokenInfo');
    localStorage.removeItem('userBasicInfo');
  }
  getPrivateToken(data) {
    var header = {
      'content-type': 'application/x-www-form-urlencoded'
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    data = str;
    return new Promise((resolve, reject) => {
      this.httpSrvc.postData(this.apiData.STR_SERVER_URL, this.apiData.STR_PUBLIC_TOKEN_URL, data, header)
        .then(res => {
          this.privateToken = res.json().access_token;
          this.tokenInfo = res.json();
          this.isLoggedIn = true;
          localStorage.setItem('token', this.privateToken);
          localStorage.setItem('tokenInfo', JSON.stringify(this.tokenInfo));
          return resolve(res);
        })
    })
  }
  getCompanies() {
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/CompanyAPI/GetCompanies", options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['arrayCompanies']);
        })
    });
  }
  getCities() {
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/CityAPI", options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['arrayCities']);
        })
    });
  }
  getDomains() {
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/DomainAPI/1", options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['arrayDomains']);
        })
    });

  }
  getCompany() {
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/CompanyAPI/GetCompanies", options)
        .map(resp => resp.json())
        .subscribe((data) => {
          let companies = data['arrayCompanies'];
          let cId = this.userData.CompanyId;
          let companyName = $.grep(companies, function(v) {
            return v.Id === cId;
          });
          companyName = companyName[0]['Name'];
          resolve(companyName);
        })
    });
  }
  getCity() {
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/CityAPI", options)
        .map(resp => resp.json())
        .subscribe((data) => {
          let cities = data['arrayCities'];
          let cId = this.userData.CityId;
          let city = $.grep(cities, function(v) {
            return v.Id === cId;
          });
          city = city[0]['Name'];
          resolve(city);
        })
    });
  }
  getFollowersInfo() {
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/GetProfileInfoByUserId/" + this.userData['UserId'], options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        })
    });
  }
  getUserWall(index) {
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "TopComment": 3,
      "PageIndex": index,
      "PageSize": 5
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetWallByUser", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data.arrayContent);
        })
    });
  }
  getUserPosts(index) {
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "TopComment": 3,
      "PageIndex": index,
      "PageSize": 5
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetMyPublish", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data.arrayContent);
        })
    });
  }
  getUserSavedPosts(index){
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "TopComment": 3,
      "PageIndex": index,
      "PageSize": 5,
      "UserContentTypeId":2
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetMyPublish", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data.arrayContent);
        })
    });
  }
  getOtherUserPosts(id,index){
    var options = this.getOptions();
    var data = {
      "UserId": id,
      "TopComment": 3,
      "PageIndex": index,
      "PageSize": 5
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetMyPublish", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data.arrayContent);
        })
    });

  }
  getCommentsForPost(postId) {
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "Top": 150,
      "ContentId": postId
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetPublishByContentId", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['arrayContent'][0]['ListComment']);
        })
    });
  }
  updateUserInfo(data) {
    var options = this.getOptions();
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/UsersAPI/UpdateUser", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          this.getUserInfo().then(info => this.userData = info);
          resolve(data);
        })
    });
  }
  getReportTypes(){
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentReportTypeAPI", options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['arrayContentReportType']);
        })
    });
  }
  reportContent(report){
    var options = this.getOptions();
    var data = {
      UserId: this.userData.UserId,
      ContentId: report.postId,
      UserContentTypeId: 3,
      ReportTypeId:report.ReportTypeId,
      ReportDescription:report.ReportDescription
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/SaveUserContent", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        })
    });
  }
  getLegends() {
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetLegendsOfTheService/" + this.userData.UserId, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['ArrayLegends']);
        })
    });
  }
  likePost(postId) {
    var options = this.getOptions();
    var data = {
      UserId: this.userData.UserId,
      ContentId: postId,
      UserContentTypeId: 1,
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/SaveUserContent", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data['CountLikes']);
        })
    });
  }
  savePost(postId) {
    var options = this.getOptions();
    var data = {
      UserId: this.userData.UserId,
      ContentId: postId,
      UserContentTypeId: 2,
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/SaveUserContent", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          var data2 = {
            "UserId": this.userData.UserId,
            "Top": 150,
            "ContentId":postId
          };
          var str = Object.keys(data2).map(function(key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(data2[key]);
          }).join('&');
          this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetPublishByContentId", str, options)
            .map(resp => resp.json())
            .subscribe((data) => {
              resolve(data['arrayContent'][0]);
            });
        })
    });
  }
  commentPosts(postId, comment) {
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "File": "",
      "Content": comment,
      "IsActive": true,
      "ContentParent": postId,
      "ContentTypeId": 5,
      "ContentDescription": comment
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/SaveContent", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          var data2 = {
            "UserId": this.userData.UserId,
            "Top": 150,
            "ContentId":postId
          };
          var str = Object.keys(data2).map(function(key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(data2[key]);
          }).join('&');
          this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetPublishByContentId", str, options)
            .map(resp => resp.json())
            .subscribe((data) => {
              resolve(data['arrayContent'][0]);
            });
        });
    });
  }
  getChallenges(index){
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "TopComment": 3,
      "PageIndex": index,
      "PageSize": 5
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ChallengeAPI/GetAllChallenges", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
            resolve(data.arrayContent);
        })
    });
  }

  // MAKE SYSTEMS
  uploadFile(file: File){
    let formData:FormData = new FormData();
    formData.append(file.name, file, file.name);

    var header = {
      'Authorization': 'bearer ' + this.privateToken
    }

    var headers = new Headers(header);
    let options = new RequestOptions({ headers: headers });
    
    return new Promise((resolve, reject) => {
      this.http.post(`${this.apiData.STR_SERVER_URL}WebAPI/api/MultimediaAPI/PostUserImage`, formData, options)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },error => {
          reject(error);
        });
    });
  }

  savePostWall(text, file, contentType){
    var options = this.getOptions();
    var data = {
      UserId: this.userData.UserId,
      File: file,
      Content: text,
      IsActive: true,
      ContentParent: "",
      ContentTypeId: contentType,
      ContentDescription: text
    }
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');

    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/SaveContent", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          console.log(data);
          resolve(data);
        })
    });
  }
  
  getPostById(postId) {
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "Top": 3,
      "ContentId": postId
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ContentAPI/GetPublishByContentId", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        }, err=>{
          reject(err);
        })
    });
  }

  getNotificatios() {
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "PageIndex": 0,
      "PageSize": 100
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/NotificationAPI/GetNotificationByUserId", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        }, err=>{
          reject(err);
        })
    });
  }

  getDetailChallengeById(idReto){
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "ChallengeId": idReto
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ChallengeAPI/GetChallengerByChallengeId", str, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        }, (err)=>{
          reject(err);
        })
    });
  }

  validateAnswer(idAnswer, idReto, swLastQuestion){
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/ChallengeAPI/GetAnswerById/" + idAnswer, options)
        .map(resp => resp.json())
        .subscribe((data) => {
          if (data.Result){
            let dataResult = data;
            this.saveAnswerByUser(idReto, idAnswer, swLastQuestion).then( data=>{
              dataResult.PointsObtained = 0;
              let saveRespuesta:any = data;
              if (saveRespuesta.Result){
                dataResult.PointsObtained = saveRespuesta.PointsObtained;
              }
              resolve(dataResult);
            }, err=>{
              reject(err);
            })
          }
          // resolve(data);
        }, (err)=>{
          reject(err);
        })
    });
  }

  saveAnswerByUser(idReto, AnswerId, swLast){
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "ChallengeId": idReto,
      "AnswerId": AnswerId,
      "IsLastQuestion": swLast
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ChallengeAPI/SaveChallengeQueryByUser", str, options)
      .map(resp => resp.json())
      .subscribe((data) => {
        resolve(data);
      }, (err)=>{
        reject(err);
      })
    });
  }

  SaveChallengeUploadFileByUser(idReto, path){
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "ChallengeId": idReto,
      "FileDescription": path,
      "FilePath": path
    };
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/ChallengeAPI/SaveChallengeUploadFileByUser", str, options)
      .map(resp => resp.json())
      .subscribe((data) => {
        resolve(data);
      }, (err)=>{
        reject(err);
      })
    });
  }

  loadAwards(){
    var options = this.getOptions();
    return new Promise((resolve, reject) => {
      this.http.get(this.apiData.STR_SERVER_URL + "WebAPI/api/AwardAPI/GetAllAwards/", options)
        .map(resp => resp.json())
        .subscribe((data) => {
          resolve(data);
        }, (err)=>{
          reject(err);
        })
    });    
  }

  redimirPremio(award, reden){
    var options = this.getOptions();
    var data = {
      "UserId": this.userData.UserId,
      "AwardId": award.Id,
      "Name": this.userData.Name,
      "Cityid": this.userData.CityId,
      "CompanyId": this.userData.CompanyId,
      "Quantity": reden.cantidad,
      "Address": reden.address,
    };
    
    var str = Object.keys(data).map(function(key) {
      return encodeURIComponent(key) + '=' + encodeURIComponent(data[key]);
    }).join('&');
    return new Promise((resolve, reject) => {
      this.http.post(this.apiData.STR_SERVER_URL + "WebAPI/api/RedemptionAPI/SaveRedemption", str, options)
      .map(resp => resp.json())
      .subscribe((data) => {
        resolve(data);
      }, (err)=>{
        reject(err);
      })
    });
  }
}

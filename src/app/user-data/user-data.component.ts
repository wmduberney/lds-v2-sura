import { Component, OnInit, Input,NgZone,ApplicationRef } from '@angular/core';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-user-data',
  templateUrl: './user-data.component.html',
  styleUrls: ['./user-data.component.sass'],
  providers:[UserService]
})
export class UserDataComponent {

  @Input() name: string;
  baseURL = this.userService.apiData.STR_SERVER_URL+'WebAPI/';
  defaultPicture = 'assets/img/profile.png';
  user;
  followersInfo;
  company = {};
  city = {};
  Follows=0;
  Followers=0;
  Votes=0;
  constructor(private userService:UserService ,public zone: NgZone,private ref: ApplicationRef) {
    this.updateFollowerStatus();
    this.user = this.userService.userData;
    this.city = this.user['City'];
    this.company = this.user['Company'];

    // this.company = this.userService.getCompany().then(company=>this.company = company);
    // this.city = this.userService.getCity().then(city=>this.city = city);
  }

  public updateFollowerStatus(){

      this.userService.getFollowersInfo().then(followers=>{
          this.Follows = followers['Follows'];
          this.Followers = followers['Followers'];
          this.Votes = followers['Votes'];
    });
  }
}

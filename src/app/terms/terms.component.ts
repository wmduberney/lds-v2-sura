import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'terms-component',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.sass'],
  providers:[UserService]
})
export class TermsComponent{
    terms = {};
    isLoggedIn = this.userService.isLoggedIn;
    constructor(private router: Router,private userService:UserService  ) {
      this.userService.getTermsAndConditions().then(terms=>{this.terms=terms;});
    }

    goBack(){
      window.history.back()
    }
    goToLogin(){
      this.router.navigate(['/login']);
    }
}

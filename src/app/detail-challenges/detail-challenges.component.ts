import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-detail-challenges',
  templateUrl: './detail-challenges.component.html',
  styleUrls: ['./detail-challenges.component.sass'],
  providers:[UserService]
})
export class DetailChallengesComponent implements OnInit {
  // @ViewChild('slickModal') slickModal: ElementRef;

  id: number;
  private sub: any;
  challenges = [];
  respDetail;
  indexAnswer = -1;
  swRequestValidate = 0;
  respAnswer;
  txtButtonAnswer = "Siguiente";
  sumPoints:number = 0;
  showDoneTrivia = false;
  aswersGood = 0;
  aswersBad = 0;
  nameFile = "Archivos en formato .jpg .png o .gif";
  nameFileVideos = "Archivos en formato .mp4";
  txtBtnReto = "Completar reto";
  mainFileUpload: File;
  commentsImage:string = "";
  respUpload;
  respPublicReto; 

  constructor(private userService: UserService,
              private router: Router,
              private route: ActivatedRoute) {

    $('.carousel').carousel({
      pause: true,
      interval: false
    });
    this.inactiveButton();
  }

  openFiles(){
    $("#myFiles").click();
  }

  public fileChangeEvent(fileInput, sw){
    if (fileInput.target.files && fileInput.target.files[0]) {
      if (sw === "img"){
        this.nameFile = fileInput.target.files[0].name;
      }else if (sw === "vid"){
        this.nameFileVideos = fileInput.target.files[0].name;
      }
      let fileList: FileList = fileInput.target.files;
      if(fileList.length > 0) {
        this.mainFileUpload = fileList[0];
      }
    }
  }

  retoImagen(){
    if (!this.mainFileUpload){
      console.log("Debes seleccionar una imagen");
      return;
    }
    // activeButtonImage()
    this.inactiveButtonImage();
    this.txtBtnReto = "Completando...";
  

    this.userService.uploadFile(this.mainFileUpload).then(data=>{
      this.respUpload = data;
      let urlFile = this.respUpload.Message;
      console.log(urlFile);

      this.userService.SaveChallengeUploadFileByUser(this.id, urlFile).then(data=>{
        this.respPublicReto = data;
        if(this.respPublicReto.Result){
          this.returnRetos();
        }
      }, err=>{

      })
    }, err=>{
      console.log("error");
      console.log(err);
    });


    console.log(this.commentsImage);
  }

  
  validateAnswerService(respuesta){
    if (this.swRequestValidate === 0){
      let answered = this.respDetail.ArrayChallenger[0].ArrayQuestion[this.indexAnswer].swAnswered;
      if (answered){
        let obj = $("#Q_" + respuesta.AnswerId);
        obj.parent().find("input:radio").attr('disabled',true);
      }else{
        this.swRequestValidate = 1;
        let obj = $("#Q_" + respuesta.AnswerId);
        obj.css("opacity", "0.5");

        let totalAsk = this.respDetail.ArrayChallenger[0].ArrayQuestion.length;
        let isDone = "false";
        if ((this.indexAnswer+1) >= totalAsk ){
          isDone = "true";
        }

        this.userService.validateAnswer( respuesta.AnswerId, this.id, isDone ).then( data=>{
          this.respAnswer = data;
          console.log(this.respAnswer);
          if (this.respAnswer.Result){
            let isC = this.respAnswer.IsCorrect;
            this.sumPoints += this.respAnswer.PointsObtained;
            this.respDetail.ArrayChallenger[0].ArrayQuestion[this.indexAnswer].swAnswered = "SI";
            this.swRequestValidate = 0;
            this.activeButton();
            obj.css("opacity", "1");
            if (isC){
              this.aswersGood++;
              obj.addClass("success");
            }else{
              this.aswersBad++;
              obj.addClass("bad");
            }
            
          }
          
        }, err=>{
          console.log("Err");
          console.log(err);
        })
      }
    }
  }

  
  activeButtonImage(){
    $("#bntReden").removeClass('busy');
    $("#bntReden").removeAttr('disabled');
  }
  inactiveButtonImage(){
    $("#bntReden").addClass('busy');
    $("#bntReden").attr('disabled','disabled');
  }

  activeButton(){
    $("#btnPublish").removeClass('busy');
    $("#btnPublish").removeAttr('disabled');
  }

  inactiveButton(){
    $("#btnPublish").addClass('busy');
    $("#btnPublish").attr('disabled','disabled');
  }

  validateAnswer(){
    let answered = this.respDetail.ArrayChallenger[0].ArrayQuestion[this.indexAnswer].swAnswered;
    if (!answered){
      this.inactiveButton();
    }else{
      let totalAsk = this.respDetail.ArrayChallenger[0].ArrayQuestion.length;
      if (totalAsk > (this.indexAnswer+1)){
        this.indexAnswer++;
        if (totalAsk == (this.indexAnswer+1)){
          this.txtButtonAnswer = 'Finalizar';
        }
        
        $('.carousel').carousel('next');
        this.inactiveButton();
      }else{
        this.showDoneTrivia = true;
      }
    }
    


  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['retoId'];
      this.loadDetail();
   });    
  }

  returnRetos(){
    this.router.navigate(['/challenges']);
  }

  loadDetail(){
    this.userService.getDetailChallengeById( this.id ).then( data=>{
      console.log(data);
      this.respDetail = data;
      this.challenges.push.apply(this.challenges,this.respDetail.ArrayChallenger);
      this.indexAnswer = 0;
    }, err=>{
      console.log("Err");
      console.log(err);
    })
  }

  



}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailChallengesComponent } from './detail-challenges.component';

describe('DetailChallengesComponent', () => {
  let component: DetailChallengesComponent;
  let fixture: ComponentFixture<DetailChallengesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailChallengesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailChallengesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});

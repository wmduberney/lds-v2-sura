import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
declare var $:any;

@Component({
  selector: 'app-update-data',
  templateUrl: './update-data.component.html',
  styleUrls: ['./update-data.component.sass'],
  providers:[UserService]
})
export class UpdateDataComponent implements OnInit {

  baseURL = this.userService.apiData.STR_SERVER_URL+'WebAPI/';
  defaultPicture = 'assets/img/profile.png';
  user={
    "Name":'',
    "Picture":''
  };
  company={
    "Name":'',
    "Id":''
  };
  city={
    "Name":'',
    "Id":''
  };
  cities = [];
  companies = [];
  domains = [];
  updateInfo = {
    "UserId": '',
    "CityId":'',
    "CompanyId":'',
    "Email":'',
    "Picture":'',
    "EmailBase":'',
    "DomainName":''
  };
  // updateInfo;
  message='';
  constructor(private userService:UserService ) {
    this.userService.getUserInfo().then(user=>{
      this.user['Name']=user['Name'];
      this.user['Picture']=user['Picture'];

    });

    this.userService.getCompany().then(company=>{
        this.company.Name = company['Name'];
        this.company.Id = company['Id'];
    });
    this.userService.getCity().then(city=>{
      this.city.Name = city['Name'];
      this.city.Id = city['Id'];
    });
    this.userService.getCompanies().then(companies=>this.companies.push.apply(this.companies, companies));
    this.userService.getCities().then(cities=>this.cities.push.apply(this.cities,cities));
    this.userService.getDomains().then(domains=>this.domains.push.apply(this.domains,domains));
    this.updateInfo ={
      "UserId": this.userService.userData['UserId'],
      "CityId": this.userService.userData['CityId'],
      "CompanyId": this.userService.userData['CompanyId'],
      "Email": this.userService.userData['Email'],
      "Picture": this.userService.userData['Picture'],
      "EmailBase": this.userService.userData['Email'].split('@')[0],
      "DomainName":'@'+this.userService.userData['Email'].split('@')[1],
    }
  }

  ngOnInit() {

  }
  updateData(e){
    e.preventDefault();
    this.message = '';
    let data = this.updateInfo;
    if(!data['CityId']){
      this.message = "Por favor seleccione una ciudad."
      return
    }
    if(!data['CompanyId']){
      this.message = "Por favor seleccione una compañia."
      return
    }
    if(!data['EmailBase']){
      this.message = "Por favor ingrese un correo."
      return
    }
    let updateData = {
      "UserId": data['UserId'],
      "CityId":data['CityId'],
      "CompanyId":data['CompanyId'],
      "Picture":data['Picture'],
      "Email":data['EmailBase']+data['DomainName']
    }
    var btn = e.currentTarget;
    $(btn).html('Actualizando...');
    $(btn).addClass('busy');
    $(btn).attr('disabled','disabled');
    this.userService.updateUserInfo(updateData).then(data=>{
      $(btn).html('Aceptar');
      $(btn).removeClass('busy');
      $(btn).removeAttr('disabled');
      this.message = "Datos actualizados correctamente."

    });
  }

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';


import { SlickModule } from 'ngx-slick';

// Rutas
import { APP_ROUTING } from './app.routes';
import { AppComponent } from './app.component';
import { TermsComponent } from './terms/terms.component';
import { FAQComponent } from './faq/faq.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { HeaderLoginComponent } from './header-login/header-login.component';
import { HeaderComponent } from './header-login/header.component';
import { UpdateDataComponent } from './update-data/update-data.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpModule } from '@angular/http';
import { SavedContentsComponent } from './saved-contents/saved-contents.component';
import { UserDataComponent } from './user-data/user-data.component';
import { LegendComponent } from './legend/legend.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { WallService } from './services/wall/wall.service';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { UserService } from './services/user/user.service';
import { HttpService } from './services/http/http.service';
import { ReportComponent } from './report/report.component';
import { ChallengesComponent } from './challenges/challenges.component';
import { RealTimeLegendsComponent } from './real-time-legends/real-time-legends.component';
import { DetailChallengesComponent } from './detail-challenges/detail-challenges.component';
import { PremiosComponent } from './premios/premios.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TermsComponent,
    FAQComponent,
    LoginComponent,
    HeaderComponent,
    HeaderLoginComponent,
    UpdateDataComponent,
    ProfileComponent,
    DashboardComponent,
    SavedContentsComponent,
    UserDataComponent,
    LegendComponent,
    UserProfileComponent,
    ReportComponent,
    ChallengesComponent,
    RealTimeLegendsComponent,
    DetailChallengesComponent,
    PremiosComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    HttpModule,
    FormsModule,
    SlickModule.forRoot()
  ],
  providers: [
    WallService,
    AuthGuardService,
    UserService,
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../services/user/user.service';
declare var $:any;

@Component({
  selector: 'report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.sass'],
  providers:[UserService]
})
export class ReportComponent implements OnInit {

  id: number;
  private sub: any;
  reportTypes = [];
  message = '';
  constructor(private route: ActivatedRoute,private userService:UserService) {
    this.userService.getReportTypes().then(reportTypes =>{this.reportTypes.push.apply(this.reportTypes,reportTypes);});
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.id = +params['postId'];

    });
  }

  goBack(){
    window.history.back()
  }
  report(){
    var reportTypeId = $('input[name=report]:checked').val();
    if(!reportTypeId){
      this.message = 'Por favor seleccione un motivo para reportar este contenido.';
      return
    }
    var message ='';
    if(reportTypeId==12){
      message = $('#reportMessage').val();
      if(!message){
        this.message = 'Por favor escriba la razon para reportar este contenido.';
        return
      }
    }
    var data = {
      postId:this.id,
      ReportDescription:message,
      ReportTypeId:reportTypeId
    };
    this.userService.reportContent(data).then(res=>this.message="Reporte recibido exitosamente.");
  }
}

import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service'
declare var $:any;

@Component({
  selector: 'app-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.sass'],
  providers:[UserService]
})
export class LegendComponent implements OnInit {

  legends = [];
  message ='';
  currentLegend:{};
  selected:Boolean;
  constructor(private userService:UserService) {
    this.selected = false;
    this.userService.getLegends().then(legends =>{
      this.legends.push.apply(this.legends,legends);
      for(var i in legends){
        this.completeUser(legends[i]['UserId']);
      }
    })
  }
  completeUser(userId){
    this.userService.getOtherUserFollowrs(userId).then(followers=>{
      for(var i in this.legends){
        if(this.legends[i]['UserId']==userId){
          this.legends[i]['Followers'] = followers['Followers'];
          this.legends[i]['Follows'] = followers['Follows'];
          this.legends[i]['Votes'] = followers['Votes'];
          break;
        }
      }
    });
    this.userService.isVoted(userId).then(isVoted=>{
      for(var i in this.legends){
        if(this.legends[i]['UserId']==userId){
          this.legends[i]['IsVoted'] = isVoted
          break;
        }
      }
    })
  }
  ngOnInit() {

  }
  followUser(userId){
    let legend = {};
    for(var i in this.legends){
      if(this.legends[i]['UserId']==userId){
        legend = this.legends[i];
        break;
      }
    }
    legend['IsFollow'] = !legend['IsFollow'];

    this.userService.followUser(userId,legend['IsFollow']).then(res=>{
      this.completeUser(userId);
    })
  }
  openModal(userId){
    this.message = ''
    $('#confirm-votin-'+userId).modal('show');
  }
  vote(userId){
    this.message = ''
    var voteMessage = $('#confirm-votin-'+userId+' .voteMessage').val();
    if(!voteMessage){
      this.message = 'Por favor escribe las razones de tu voto.';
      return;
    }

    this.userService.voteForUser(userId,voteMessage).then(result=>{
      $('#confirm-votin-'+userId).modal('hide');
      this.completeUser(userId);
    });
  }

}

import { Component } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'faq-component',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.sass'],
  providers:[UserService]

})
export class FAQComponent {
    faqs = [];
    isLoggedIn = this.userService.isLoggedIn;
    constructor(private router: Router,private userService:UserService ) {
      this.userService.getFAQs().then(faqs=>this.faqs.push.apply(this.faqs,faqs));
    }goBack(){
      window.history.back()
    }
    goToLogin(){
      this.router.navigate(['/login']);
    }
}

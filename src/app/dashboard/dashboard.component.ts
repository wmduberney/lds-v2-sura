import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass'],
  providers:[UserService]
})

export class DashboardComponent implements OnInit {

  posts = [];
  gettingMorePosts = false;
  currentPage = 0;
  user = {}
  nameFile = "Archivos en formato .jpg .png o .gif";
  nameFileVideos = "Archivos en formato .mp4";
  showUploadPhoto:boolean = false;
  showUploadVideo:boolean = false;
  commentUser:string = "";
  mainFileUpload: File;
  respUpload: any = {};
  respPublicPost;
  respPostById;
  mainContentType: number = 1;
  
  constructor(private router: Router,private userService: UserService) {
    this.user = this.userService.userData;
    this.userService.getUserWall(this.currentPage).then(posts => { 
      this.posts.push.apply(this.posts, posts);
    });
  }

  openFiles(){
    $("#myFiles").click();
  }
  openFilesVideos(){
    $("#myFilesVideos").click();
  }

  activeView(swContent){
    this.mainContentType = swContent;
    if (this.mainContentType === 3){
      this.showUploadPhoto = true;
    }else{
      this.showUploadPhoto = false;
    }
    if (this.mainContentType === 2){
      this.showUploadVideo = true;
    }else{
      this.showUploadVideo = false;
    }
  }

  public fileChangeEvent(fileInput, sw){
    if (fileInput.target.files && fileInput.target.files[0]) {
      if (sw === "img"){
        this.nameFile = fileInput.target.files[0].name;
      }else if (sw === "vid"){
        this.nameFileVideos = fileInput.target.files[0].name;
      }
      let fileList: FileList = fileInput.target.files;
      if(fileList.length > 0) {
        this.mainFileUpload = fileList[0];
      }
    }
  }

  publicPost(){
    $("#btnPublish").html('Publicando...');
    $("#btnPublish").addClass('busy');
    $("#btnPublish").attr('disabled','disabled');

    if (this.mainContentType === 2 || this.mainContentType === 3){
      this.userService.uploadFile(this.mainFileUpload).then(data=>{
        this.respUpload = data;
        let urlFile = this.respUpload.Message;
        this.userService.savePostWall(this.commentUser, urlFile, this.mainContentType).then(data=>{
          this.respPublicPost = data;
          this.activePublish();
        }, err=>{
        })
      }, err=>{
        console.log("error");
        console.log(err);
      });
    }else{
      this.userService.savePostWall(this.commentUser, "", this.mainContentType).then(data=>{
        this.respPublicPost = data;
        this.activePublish();
      }, err=>{

      })
    }
  }

  activePublish(){
    this.userService.getPostById(this.respPublicPost.ContentId).then(data=>{
      this.respPostById = data;
      if (this.respPostById.Result && this.respPostById.arrayContent.length>0){
        this.posts.unshift(this.respPostById.arrayContent[0]);
      }
      this.commentUser = "";
      $("#btnPublish").html('Publicar');
      $("#btnPublish").removeClass('busy');
      $("#btnPublish").removeAttr('disabled');
    }, err=>{
      console.log("Error");
      console.log(err);
    })
  }

  likePost( postId) {
    this.userService.likePost(postId).then(count => {
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex].CountLike = count;
      this.posts[objIndex].IsLike = !this.posts[objIndex].IsLike;
    });
  }
  savePost(postId){
    this.userService.savePost(postId).then(post => {
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex] = post;
    });
  }
  commentPosts(postId){
    var comment = $('#publication_'+postId+' .comment').val();
    if(!comment){
      return;
    }
    this.userService.commentPosts(postId,comment).then(post => {
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex] = post;
    });
  }
  ngOnInit() {
    window.addEventListener('scroll', this.scroll, true);
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (e): void => {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 250) {
      if (!this.gettingMorePosts) {
        this.gettingMorePosts = true;
        this.currentPage += 1;
        this.userService.getUserWall(this.currentPage).then(posts => {
          this.posts.push.apply(this.posts, posts);
          this.gettingMorePosts = false;
        });
      }
    }
  };
  showCommentsForPost(postId){
    $('#publication_'+postId+' .commentsHolder').toggleClass('hidden');
  }
  showMoreCommentsForPost(postId){
    this.userService.getCommentsForPost(postId).then(comments=>{
      let objIndex = this.posts.findIndex((obj => obj.ContentId == postId));
      this.posts[objIndex].ListComment = comments;
    });
    $('#publication_'+postId+' .showMoreText').addClass('hidden');
  }
      reportContent(postId){
        this.router.navigate(['/report',postId]);
      }
}

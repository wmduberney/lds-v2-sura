import { Component ,OnInit  } from '@angular/core';
import { Title }     from '@angular/platform-browser';
import { HttpService } from './services/http/http.service';
declare var $:any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  providers:[ HttpService]
})

export class AppComponent implements OnInit{
  public constructor(private titleService: Title ) { }

  public setTitle( newTitle: string) {
    this.titleService.setTitle( newTitle );
  }
  ngOnInit() {
      this.titleService.setTitle('Leyendas Del Servicio');
  }
}

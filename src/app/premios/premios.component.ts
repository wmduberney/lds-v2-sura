import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';

declare var $: any;

@Component({
  selector: 'app-premios',
  templateUrl: './premios.component.html',
  styleUrls: ['./premios.component.sass'],
  providers:[UserService]
})
export class PremiosComponent implements OnInit {
  premios = [];
  respPremios;
  respReden;
  user;
  showMaskReden:boolean = false;
  reden:any = {};
  premioSelected;


  constructor(private userService: UserService) {
    this.user = this.userService.userData;
    console.log(this.user);

    this.userService.loadAwards().then( data=>{
      this.respPremios = data;
      if (this.respPremios.Result){
        this.premios = this.respPremios.ArrayAwards;
        console.log(this.premios);
      }
    }, err=>{
      console.log("Err");
      console.log(err);
    });
  }


  redimir(){
    if ( this.reden.cantidad == "" || this.reden.address == ""  ){
      return;
    }else if ( !this.reden.cantidad || !this.reden.address ){
      return;
    }

    this.userService.redimirPremio( this.premioSelected, this.reden ).then( data=>{
      console.log("Respuesta redencion")
      this.respReden = data;
      if (this.respReden.Result){
        this.hideMaskReden();
      }
      console.log(data);
    }, err=> {
      console.log("Err");
      console.log(err);
    })
  }

  ngOnInit() {
  }

  showReden(premio){
    this.premioSelected = premio;
    this.showMaskReden = true;
  }

  hideMaskReden(){
    this.showMaskReden = false;
  }


}

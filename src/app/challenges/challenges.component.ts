import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-challenges',
  templateUrl: './challenges.component.html',
  styleUrls: ['./challenges.component.sass'],
  providers:[UserService]

})
export class ChallengesComponent implements OnInit {
  challenges = [];
  currentIndex = 0;
  constructor(private userService: UserService, private router: Router) {
    this.userService.getChallenges(this.currentIndex).then(challenges=>{
      this.challenges.push.apply(this.challenges,challenges);
      console.log(this.challenges);
    });
  }

  ngOnInit() {
  }

  loadDetailReto(reto){
    console.log(reto);
    this.router.navigate(['/detail-challenge', reto.ChallengeId ]);
  }

}

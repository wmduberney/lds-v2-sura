import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { UpdateDataComponent } from './update-data/update-data.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SavedContentsComponent } from './saved-contents/saved-contents.component';
import { LegendComponent } from './legend/legend.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { TermsComponent } from './terms/terms.component';
import { FAQComponent } from './faq/faq.component';
import { ReportComponent } from './report/report.component';
import { RealTimeLegendsComponent } from './real-time-legends/real-time-legends.component';
import { ChallengesComponent } from './challenges/challenges.component';
import { DetailChallengesComponent } from './detail-challenges/detail-challenges.component';
import { PremiosComponent } from './premios/premios.component';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent, canActivate:[AuthGuardService] },
  { path: 'update-data', component: UpdateDataComponent, canActivate: [AuthGuardService]  },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService]  },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]  },
  { path: 'saved-contents', component: SavedContentsComponent, canActivate: [AuthGuardService]  },
  { path: 'legend', component: LegendComponent },
  { path: 'user-profile/:id', component: UserProfileComponent, canActivate: [AuthGuardService]  },
  { path: 'terminosycondiciones',component: TermsComponent },
  { path: 'faq',component: FAQComponent },
  { path: 'report/:postId', component: ReportComponent, canActivate:[AuthGuardService] },
  { path: 'challenges', component: ChallengesComponent, canActivate:[AuthGuardService] },
  { path: 'awards', component: PremiosComponent, canActivate:[AuthGuardService] },
  { path: 'detail-challenge/:retoId', component: DetailChallengesComponent, canActivate:[AuthGuardService] },
  
  { path: 'real-time-legends', component: RealTimeLegendsComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: true });

import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user/user.service';
import { HttpService } from '../services/http/http.service';
import { NgModel } from '@angular/forms';
import { Router } from '@angular/router';
declare var $ :any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass'],
  providers: [
    HttpService, UserService,
  ]
})
export class LoginComponent implements OnInit {

  public loginData = {
    UserName: "",
    Password: "",
    IpAddress: "",
    ProgramId: "1",
    UserTypeId: "1"
  }
  public message = '';
  constructor(
    private userService: UserService,
    private router:Router) {

  }

  ngOnInit() {
    this.getToken();
  }

  getToken(){

    this.userService.getPublicToken()
    .then(res => {
    })
    .catch(res => {
      console.log(res);
    })
  }

  onKey(event){
    if (event.keyCode === 13){
      this.loginEnter();
    }
  }

  loginEnter(){
    var btn = $("#buttonLogin");
    $(btn).html('Ingresando...');
    $(btn).addClass('loggingIn');
    $(btn).attr('disabled','disabled');
    this.userService.login(this.loginData)
    .then(data => {
        if(!data['Result']){
          this.message = 'Nombre de usuario o contraseña equivocada';
          $(btn).html('Ingresar');
          $(btn).removeClass('loggingIn');
          $(btn).removeAttr('disabled');
          return;
        }
        this.router.navigate(['/dashboard']);
    })
    .catch(res => {
      console.log (res);
    })
  }  

  login(event){
    event.preventDefault();
    var btn = event.currentTarget;
    $(btn).html('Ingresando...');
    $(btn).addClass('loggingIn');
    $(btn).attr('disabled','disabled');
    this.userService.login(this.loginData)
    .then(data => {
        if(!data['Result']){
          this.message = 'Nombre de usuario o contraseña equivocada';
          $(btn).html('Ingresar');
          $(btn).removeClass('loggingIn');
          $(btn).removeAttr('disabled');
          return;
        }
        this.router.navigate(['/dashboard']);
    })
    .catch(res => {
      console.log (res);
    })
  }

}
